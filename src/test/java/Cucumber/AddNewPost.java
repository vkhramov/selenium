package Cucumber;

import core.BaseTest;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import enums.Browsers;
import junit.framework.Assert;

public class AddNewPost extends BaseTest{

    private static class T{
        public static final String postTitle = "Post Title";
        public static final String postBody = "Post Body";
    }

    @Given("^opened Main Page of WordPress Admin$")
    public void opened_Main_Page_of_WordPress_Admin(){
        Assert.assertTrue(getCurrentDriver().getTitle().contains("Dashboard"));
    }

    @When("^press Posts menu item on Admin Toolbar on the right side of the screen$")
    @And("^press on Add New sub menu item on pop up sub menu$")
    public void press_Posts_menu_item_on_Admin_Toolbar_on_the_right_side_of_the_screen(){
        addNewPostPage.AddNewPostFromAdminToolbar();
    }

    @And("^add new post Title and Post Body$")
    public void add_new_post_Title_and_Post_Body() {
        addNewPostPage.ProvidePostTitle(T.postTitle);
        addNewPostPage.ProvidePostBody(T.postBody);
    }

    @And("^click on Publish button$")
    public void click_on_Publish_button() {
        addNewPostPage.PressPublishButton();
    }

    @Then("^new post will be created and published$")
    public void new_post_will_be_created_and_published() {
        addNewPostPage.WaitWhilePostWillBePublished();
        editPostPage.PressViewPostLink();
        viewPostPage.WaitViewPostPage();
        viewPostPage.CheckPostTitle(T.postTitle);
    }
}
