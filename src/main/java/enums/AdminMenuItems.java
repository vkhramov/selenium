package enums;

public enum AdminMenuItems {
    Posts,
    Media,
    Pages,
    Comments
}
