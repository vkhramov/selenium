package core;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class WebDriverExtension {
    protected static WebDriver driver;

    public WebDriverExtension(WebDriver driver) {
        this.driver = driver;
    }

    protected static void MoveMouseOverElement(By element){
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(element)).perform();
    }

    protected static void MoveMouseOverElement(WebElement element){
        Actions action = new Actions(driver);
        action.moveToElement(element).perform();
    }

    protected static void SetTextToElement(By element, String text){
        Actions action = new Actions(driver);
        action.sendKeys(driver.findElement(element), text).perform();
    }

    protected static void SetTextToElement(WebElement element, String text){
        Actions action = new Actions(driver);
        action.sendKeys(element, text).perform();
    }

    protected static void ClearAndSetTextToElement(WebElement element, String text){
        Actions action = new Actions(driver);
        element.clear();
        action.sendKeys(element, text).perform();
    }

    public Object ExecuteScript(WebDriver driver, String script) {
        return ((JavascriptExecutor)driver).executeScript(script);
    }
}
