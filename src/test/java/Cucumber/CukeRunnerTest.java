package Cucumber;
import core.BaseTest;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import enums.Browsers;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import java.util.concurrent.TimeUnit;


@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        format = { "json:target/cucumber.json" },
        features = "src/main/features/")
public class CukeRunnerTest extends BaseTest{

    @BeforeClass
    public static void setUp(){
        loginPage.Open();
        getCurrentDriver().manage().window().maximize();
        loginPage.ProvideUserName();
        loginPage.ProvidePassword();
        loginPage.ClickOnLoginButton();
        getCurrentDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDown(){
        getCurrentDriver().close();
    }

}
