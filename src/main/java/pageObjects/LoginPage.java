package pageObjects;

import core.ApplicationSettings;
import core.UserCredentials;
import core.WebDriverExtension;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends WebDriverExtension {

    @FindBy(id="wp-submit")
    protected WebElement loginButton;
    @FindBy(id="user_login")
    protected WebElement usernameInput;
    @FindBy(id="user_pass")
    protected WebElement passwordInput;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void Open() {
        driver.navigate().to(ApplicationSettings.loginUrl);
    }

    public void ProvideUserName() {
        usernameInput.sendKeys(UserCredentials.userName);
    }

    public void ProvidePassword() {
        passwordInput.sendKeys(UserCredentials.userPassword);
    }

    public void ClickOnLoginButton() {
        loginButton.click();
    }
}
