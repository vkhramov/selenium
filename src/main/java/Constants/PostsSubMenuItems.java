package Constants;

public class PostsSubMenuItems {
    public static String AddNew = "//a[text()='Add New']";
    public static String AllPosts = "//a[text()='All Posts']";
    public static String Categories = "//a[text()='Categories']";
    public static String Tags = "//a[text()='Tags']";

}
