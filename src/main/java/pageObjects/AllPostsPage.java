package pageObjects;

import Constants.PostsSubMenuItems;
import Constants.RowActions;
import core.WebDriverExtension;
import enums.AdminMenuItems;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AllPostsPage extends WebDriverExtension {

    private AdminMenuToolbarPage _adminMenuToolbarPage = PageFactory.initElements(driver, AdminMenuToolbarPage.class);
    private SubMenuPage _subMenuPage = new SubMenuPage(driver);

    @FindBy(id="message")
    WebElement infoMessage;

    public AllPostsPage(WebDriver driver){
        super(driver);
    }

    public void OpenAllPostsFromAdminToolbar(){
        _adminMenuToolbarPage.MoveMouseToMenuItem(AdminMenuItems.Posts);
        _subMenuPage.SelectSubMenuItem(PostsSubMenuItems.AllPosts);
    }

    public void EditPost(String postTitle){
        WebElement currentPostRow = driver.findElement(By.xpath("//a[text()='" + postTitle + "']"));
        MoveMouseOverElement(currentPostRow);
        ClickLink(RowActions.Edit, currentPostRow);
    }

    public void EditPost(int postIndex){
        WebElement currentPostRow = driver.findElement(By.id("post-"+postIndex));
        WebElement rowTitle = currentPostRow.findElement(By.className("row-title"));
        MoveMouseOverElement(rowTitle);
        ClickLink(RowActions.Edit, currentPostRow);
    }

    public void DeletePost(String postTitle){
        WebElement currentPostRow = driver.findElement(By.xpath("//a[text()='" + postTitle + "']"));
        MoveMouseOverElement(currentPostRow);
        ClickLink(RowActions.Trash, currentPostRow);
    }

    public void DeletePost(int postIndex){
        WebElement currentPostRow = driver.findElement(By.id("post-"+postIndex));
        WebElement rowTitle = currentPostRow.findElement(By.className("row-title"));
        MoveMouseOverElement(rowTitle);
        ClickLink(RowActions.Trash, currentPostRow);
    }

    private void ClickLink(String rowAcion, WebElement rowWebElement){
        WebElement editLink = rowWebElement.findElement(By.xpath("//a[text()='"+rowAcion+"']"));
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(editLink));
        MoveMouseOverElement(editLink);
        editLink.click();
    }

    public void CheckThatPostsWereMovedToTrash(int numberOfPosts){
        WebElement infoMessageText = infoMessage.findElement(By.tagName("p"));
        new WebDriverWait(driver, 15).until(ExpectedConditions.textToBePresentInElement(infoMessageText, numberOfPosts+" post moved to the Trash. "));
    }
}
