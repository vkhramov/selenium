package pageObjects;

import core.WebDriverExtension;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditPostPage extends WebDriverExtension {

    @FindBy(name="post_title")
    protected WebElement PostTitle;

    @FindBy(id="content_ifr")
    protected WebElement PostBodyFrame;

    @FindBy(id="publish")
    protected WebElement UpdateButton;

    @FindBy(xpath="//a[text()='View post']")
    protected WebElement ViewPostLink;

    public EditPostPage(WebDriver driver){
        super(driver);
    }

    public void EditPostTitle(String title){
        this.ClearAndSetTextToElement(PostTitle, title);
    }

    public void EditPostBody(String bodyText){
        driver.switchTo().frame(PostBodyFrame);
        driver.switchTo().activeElement().clear();
        driver.switchTo().activeElement().sendKeys(bodyText);
        driver.switchTo().defaultContent();
    }

    public void PressUpdateButton(){
        new WebDriverWait(driver, 15).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                if (!UpdateButton.getAttribute("class").toString().contains("disabled")){
                    UpdateButton.click();
                    return true;
                }
                else
                    return false;
            }
        });
    }

    public void WaitWhilePostWillBeUpdated(){
        new WebDriverWait(driver, 15).until(ExpectedConditions.titleContains("Edit Post"));
    }

    public void PressViewPostLink(){
        ViewPostLink.click();
    }
}
