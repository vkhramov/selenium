package HomeWork;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class Task2 {

    WebDriver driver;
    private static class T{
        public static final String UserName = "viktor";
        public static final String Password = "Lnr*hKCdX*atMMET&O";
        public static final String PostTitle = "My Post Title";
        public static final String PostBody = "My Post Body";
    }

    @BeforeSuite(alwaysRun = true)
    public void setupBeforeSuite(ITestContext context) {

        //System.setProperty("webdriver.chrome.driver", ".\\BrowserDrivers\\chromedriver_win32\\chromedriver.exe");
        //driver = new ChromeDriver();

        //System.setProperty("webdriver.firefox.driver", ".\\BrowserDrivers\\chromedriver_win32\\selenium-firefox-driver-2.0a5.jar");
//        File file = new File("firebug-2.0.12.xpi");
//        FirefoxProfile firefoxProfile = new FirefoxProfile();
//        try {
//            firefoxProfile.addExtension(file);
//        } catch (IOException e) {
//            System.out.println("Cannot add extension to firefox profile");
//        }
//        firefoxProfile.setPreference("extensions.firebug.currentVersion", "2.0.12"); // Avoid startup screen
//        driver = new FirefoxDriver(firefoxProfile);
        driver = new FirefoxDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterSuite(alwaysRun = true)
    public void setupAfterSuite() {
        driver.quit();
    }

    @Test(description="Login to WordPress Admin", invocationCount = 1)
    public void s1_LoginToWordPressAdmin() {
        driver.get("http://localhost:26006/wp-login.php");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals(true, driver.getTitle().toString().contains("vkhramov WordPress Site"));
        WebElement userNameEdit = driver.findElement(By.id("user_login"));
        userNameEdit.sendKeys(T.UserName);
        WebElement passwordEdit = driver.findElement(By.id("user_pass"));
        passwordEdit.sendKeys(T.Password);
        WebElement logInButton = driver.findElement(By.id("wp-submit"));
        logInButton.click();
        new WebDriverWait(driver, 20).until(ExpectedConditions.titleContains("Dashboard"));
        Assert.assertEquals(true, driver.getTitle().toString().contains("Dashboard"));
    }

    @Test(description="Add Post", invocationCount = 1)
    public void s2_AddPost() {
        System.out.println("Page Title is " + driver.getTitle());

        WebElement postButton = driver.findElement(By.xpath("//div[text()='Posts']"));
        postButton.click();

        new WebDriverWait(driver, 5).until(ExpectedConditions.titleContains("Posts"));
        Assert.assertEquals(true, driver.getTitle().toString().contains("Posts"));

        WebElement addNewPostLink = driver.findElement(By.xpath("//h1[text()='Posts ']/a"));
        addNewPostLink.click();

        new WebDriverWait(driver, 5).until(ExpectedConditions.titleContains("Add New Post"));
        Assert.assertEquals(true, driver.getTitle().toString().contains("Add New Post"));

        WebElement postTitleEdit = driver.findElement(By.name("post_title"));
        postTitleEdit.sendKeys(T.PostTitle + " [" + new Double(Math.random() * 1000).intValue() + "]");

        WebElement editorBodyFrame = driver.findElement(By.id("content_ifr"));
        driver.switchTo().frame(editorBodyFrame);
        WebElement editor = driver.findElement(By.tagName("body"));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].innerHTML = '"+T.PostBody+"'", editor);

        driver.switchTo().defaultContent();
        WebElement publishButton = driver.findElement(By.id("publish"));
        publishButton.click();

        new WebDriverWait(driver, 15).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"message\"]/p")));
    }
}
