package pageObjects;

import Constants.PostsSubMenuItems;
import core.WebDriverExtension;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SubMenuPage extends WebDriverExtension{

    private By subMenuItem;

    public SubMenuPage(WebDriver driver) {
        super(driver);
    }

    public void SelectSubMenuItem(String subMenuItemXpath){
        subMenuItem = By.xpath(subMenuItemXpath);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(driver.findElement(subMenuItem)));
        driver.findElement(subMenuItem).click();
    }
}
