package pageObjects;

import core.ApplicationSettings;
import core.WebDriverExtension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashBoardPage extends WebDriverExtension{
    public DashBoardPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath="//*[@id=\"menu-dashboard\"]/a/div[3]")
    protected WebElement DashBoardMenuItem;

    public void ReturnToDashBoard(){
        driver.navigate().to(ApplicationSettings.loginUrl);
    }

    public void WaitDahsBoardPage(){
        new WebDriverWait(driver, 15).until(ExpectedConditions.titleContains("Dashboard"));
    }
}
