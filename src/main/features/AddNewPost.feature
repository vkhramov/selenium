Feature: Add New Post
  Background: Opened WordPress Admin
    Given opened Main Page of WordPress Admin
  Scenario: Add New Post with legal Title and Body
    When press Posts menu item on Admin Toolbar on the right side of the screen
    And press on Add New sub menu item on pop up sub menu
    And add new post Title and Post Body
    And click on Publish button
    Then new post will be created and published

#  Scenario: Add New Post with empty Body
#    When press Posts menu item on Admin Toolbar on the right side of the screen
#    And press on Add New sub menu item on pop up sub menu
#    And add new post Title
#    And click on Publish button
#    Then new post will be created and published
#
#  Scenario: Add New Post with empty Title
#    When press Posts menu item on Admin Toolbar on the right side of the screen
#    And press on Add New sub menu item on pop up sub menu
#    And add new post Title
#    And click on Publish button
#    Then new post will be created and published
