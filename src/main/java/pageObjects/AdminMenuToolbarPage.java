package pageObjects;

import core.WebDriverExtension;
import enums.AdminMenuItems;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdminMenuToolbarPage extends WebDriverExtension {
    public AdminMenuToolbarPage(WebDriver driver){
        super(driver);
    }

    @FindBy(id="menu-posts")
    protected WebElement PostsMenuItem;

    @FindBy(id="menu-media")
    protected WebElement MediaMenuItem;

    @FindBy(id="menu-comments")
    protected WebElement CommentsMenuItem;

    @FindBy(id="menu-pages")
    protected WebElement PagesMenuItem;

    public void MoveMouseToMenuItem(AdminMenuItems item){
        switch (item){
            case Posts:
                this.MoveMouseOverElement(PostsMenuItem);
                break;
            case Media:
                this.MoveMouseOverElement(MediaMenuItem);
                break;
            case Pages:
                this.MoveMouseOverElement(PagesMenuItem);
                break;
            case Comments:
                this.MoveMouseOverElement(CommentsMenuItem);
                break;
            default:
                break;
        }
    }

    public void SelectMenuItem(AdminMenuItems item){
        switch (item){
            case Posts:
                PostsMenuItem.click();
                break;
            case Media:
                MediaMenuItem.click();
                break;
            case Pages:
                PagesMenuItem.click();
                break;
            case Comments:
                CommentsMenuItem.click();
                break;
            default:
                break;
        }
    }
}
