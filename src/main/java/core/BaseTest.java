package core;

import enums.Browsers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import pageObjects.*;

import java.util.concurrent.TimeUnit;

import static enums.Browsers.*;

public class BaseTest {
    private static WebDriver _driver;
    private static Browsers _browser = Chrome;

    private static final String CHROME_DRIVER_PATH = ".\\src\\browserDrivers\\chromedriver.exe";
    private static final String CHROME_DRIVER_NAME = "webdriver.chrome.driver";

    public static LoginPage loginPage = PageFactory.initElements(getCurrentDriver(), LoginPage.class);
    public static AddNewPostPage addNewPostPage = PageFactory.initElements(getCurrentDriver(), AddNewPostPage.class);
    public static EditPostPage editPostPage = PageFactory.initElements(getCurrentDriver(), EditPostPage.class);
    public static ViewPostPage viewPostPage = PageFactory.initElements(getCurrentDriver(), ViewPostPage.class);
    public static AllPostsPage allPostsPage = PageFactory.initElements(getCurrentDriver(), AllPostsPage.class);
    public static DashBoardPage dashBoardPage = PageFactory.initElements(getCurrentDriver(), DashBoardPage.class);

    private static class BrowserCleanup implements Runnable {
        public void run() {
            close();
        }
    }

    public static void close() {
        try {
            getCurrentDriver().quit();
            _driver = null;
        } catch (UnreachableBrowserException e) {
        }
    }

    protected static WebDriver getCurrentDriver() {
        if (_driver == null) {
            try {
                switch (getCurrentBrowser()) {
                    case Chrome:
                        System.setProperty(CHROME_DRIVER_NAME, CHROME_DRIVER_PATH);
                        _driver = new ChromeDriver();
                        break;
                    case Firefox:
                        _driver = new FirefoxDriver();
                        break;
                    case IE:
                        _driver = new InternetExplorerDriver();
                        break;
                    default:
                        break;
                }
                _driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            }
            finally{
                Runtime.getRuntime().addShutdownHook(
                        new Thread(new BrowserCleanup()));
            }
        }
        return _driver;
    }

    protected static Browsers getCurrentBrowser(){
        return _browser;
    }

    protected static void setCurrentBrowser(Browsers browser){
        _browser = browser;
    }
}
