package HomeWork;

import core.BaseTest;
import enums.Browsers;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pageObjects.AddNewPostPage;
import pageObjects.EditPostPage;
import pageObjects.LoginPage;
import pageObjects.ViewPostPage;

public class Task3 extends BaseTest {

    private static class T{
        public static final String postTitle = "Post Title";
        public static final String postBody = "Post Body";
    }

    @BeforeSuite
    private void OpenStartPage(){
        setCurrentBrowser(Browsers.Firefox);
        getCurrentDriver().manage().window().maximize();
        loginPage.Open();
        loginPage.ProvideUserName();
        loginPage.ProvidePassword();
        loginPage.ClickOnLoginButton();
    }

    @AfterSuite
    private void CloseBrowser(){
        getCurrentDriver().quit();
    }

//    @Test(description="Login To WordPress Admin")
//    public void S0_LoginToWordPressAdmin()
//    {
//        setCurrentBrowser(Browsers.Firefox);
//        //LoginPage loginPage = new LoginPage(getCurrentDriver());
//        loginPage.Open();
//        loginPage.ProvideUserName();
//        loginPage.ProvidePassword();
//        loginPage.ClickOnLoginButton();
//    }

    @Test(description="Add New Post")
    public void S1_AddNewPost(){
        addNewPostPage.AddNewPostFromAdminToolbar();
        addNewPostPage.ProvidePostTitle(T.postTitle);
        addNewPostPage.ProvidePostBody(T.postBody);
        addNewPostPage.PressPublishButton();
        addNewPostPage.WaitWhilePostWillBePublished();
    }

    @Test(description="View Post")
    public void S2_ViewPost(){
        editPostPage.PressViewPostLink();
        viewPostPage.WaitViewPostPage();
        viewPostPage.CheckPostTitle(T.postTitle);
    }

}
