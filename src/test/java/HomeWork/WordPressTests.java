package HomeWork;

import core.BaseTest;
import enums.Browsers;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class WordPressTests extends BaseTest {

    private static class T{
        public static final String postTitle = "Post Title";
        public static final String postBody = "Post Body";
        public static final String newPostTitle = "Post Title Updated";
    }

    @BeforeSuite
    private void OpenStartPage(){
        //setCurrentBrowser(Browsers.Firefox);
        setCurrentBrowser(Browsers.Chrome);
        getCurrentDriver().manage().window().maximize();
        loginPage.Open();
        loginPage.ProvideUserName();
        loginPage.ProvidePassword();
        loginPage.ClickOnLoginButton();
    }

    @AfterSuite
    private void CloseBrowser(){
        getCurrentDriver().quit();
    }

//    @Test(description="Login To WordPress Admin")
//    public void S0_LoginToWordPressAdmin()
//    {
//        setCurrentBrowser(Browsers.Firefox);
//        //LoginPage loginPage = new LoginPage(getCurrentDriver());
//        loginPage.Open();
//        loginPage.ProvideUserName();
//        loginPage.ProvidePassword();
//        loginPage.ClickOnLoginButton();
//    }

    @Test(description="Add New Post")
    public void S1_AddNewPost(){
        addNewPostPage.AddNewPostFromAdminToolbar();
        addNewPostPage.ProvidePostTitle(T.postTitle);
        addNewPostPage.ProvidePostBody(T.postBody);
        addNewPostPage.PressPublishButton();
        addNewPostPage.WaitWhilePostWillBePublished();
    }

    @Test(description="View Post")
    public void S2_ViewPost(){
        editPostPage.PressViewPostLink();
        viewPostPage.WaitViewPostPage();
        viewPostPage.CheckPostTitle(T.postTitle);
        dashBoardPage.ReturnToDashBoard();
        dashBoardPage.WaitDahsBoardPage();
    }

    @Test(description = "Edit Post" )
    public void S3_EditPost(){
        allPostsPage.OpenAllPostsFromAdminToolbar();
        allPostsPage.EditPost(T.postTitle);
        editPostPage.EditPostTitle(T.newPostTitle);
        editPostPage.PressUpdateButton();
        editPostPage.WaitWhilePostWillBeUpdated();
    }

    @Test(description="View Post")
    public void S4_ViewPost(){
        editPostPage.PressViewPostLink();
        viewPostPage.WaitViewPostPage();
        viewPostPage.CheckPostTitle(T.newPostTitle);
        dashBoardPage.ReturnToDashBoard();
        dashBoardPage.WaitDahsBoardPage();
    }

    @Test(description = "Delete Post" )
    public void S5_DeletePost(){
        allPostsPage.OpenAllPostsFromAdminToolbar();
        allPostsPage.DeletePost(T.newPostTitle);
        allPostsPage.CheckThatPostsWereMovedToTrash(1);
        dashBoardPage.ReturnToDashBoard();
        dashBoardPage.WaitDahsBoardPage();
    }
}
