Feature: Edit Post
  Background: Opened WordPress Admin
    Given opened Main Page of WordPress Admin and some post already created
  Scenario: Edit Post Title
    When press Posts menu item on Admin Toolbar on the right side of the screen
    And press on All Posts sub menu item on pop up sub menu
    And hover mouse over some post title
    And click on Edit action link
    And update post Title
    And press Update button
    Then post title is updated