package pageObjects;

import core.WebDriverExtension;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ViewPostPage extends WebDriverExtension{

    @FindBy(className = "entry-title")
    protected WebElement PostTitle;

    public ViewPostPage(WebDriver driver){
        super(driver);
    }

    public WebElement GetPostTitle(){
        return PostTitle;
    }

    public String GetPostTitleText(){
        return GetPostTitle().getText();
    }

    public void CheckPostTitle(String title){
        Assert.assertTrue(GetPostTitleText().equals(title), String.format("Post Title: expected is %1$s; actual is %2$s", title, GetPostTitleText()));
    }

    public void WaitViewPostPage(){
        new WebDriverWait(driver, 15).until(ExpectedConditions.presenceOfElementLocated(By.id("page")));
    }
}
