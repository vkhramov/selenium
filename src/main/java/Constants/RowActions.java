package Constants;

/**
 * Created by vkhramov on 05.10.2015.
 */
public class RowActions {
    public static String Edit = "Edit";
    public static String QuickEdit = "Quick Edit";
    public static String Trash = "Trash";
    public static String View = "View";
}
