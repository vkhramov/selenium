package pageObjects;

import Constants.PostsSubMenuItems;
import core.WebDriverExtension;
import enums.AdminMenuItems;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddNewPostPage extends WebDriverExtension {

    @FindBy(name="post_title")
    protected WebElement PostTitle;

    @FindBy(id="content_ifr")
    protected WebElement PostBodyFrame;

    @FindBy(id="publish")
    protected WebElement PublishButton;

    private AdminMenuToolbarPage _adminMenuToolbarPage = PageFactory.initElements(driver, AdminMenuToolbarPage.class);
    private SubMenuPage _subMenuPage = new SubMenuPage(driver);

    public AddNewPostPage(WebDriver driver) {
        super(driver);
    }

    public void ProvidePostTitle(String title){
        this.SetTextToElement(PostTitle, title);
    }

    public void ProvidePostBody(String bodyText){
        //driver.switchTo().frame(driver.findElement(PostBodyFrame));
        driver.switchTo().frame(PostBodyFrame);
        driver.switchTo().activeElement().sendKeys(bodyText);
        driver.switchTo().defaultContent();
    }

    public void PressPublishButton(){
        new WebDriverWait(driver, 15).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                //WebElement button = driver.findElement(PublishButton);
                if (!PublishButton.getAttribute("class").toString().contains("disabled")){
                    //button.click();
                    PublishButton.click();
                    return true;
                }
                else
                    return false;
            }
        });
    }

    public void AddNewPostFromAdminToolbar(){
        _adminMenuToolbarPage.MoveMouseToMenuItem(AdminMenuItems.Posts);
        _subMenuPage.SelectSubMenuItem(PostsSubMenuItems.AddNew);
    }

    public void WaitWhilePostWillBePublished(){
        new WebDriverWait(driver, 15).until(ExpectedConditions.titleContains("Edit Post"));
    }

}
